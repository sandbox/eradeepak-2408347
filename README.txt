
/**
 *  @file
 *  README for the Shiphero API  Module.
 */

About
-----
This module allows for integration with Shiphero.


Installation instructions
-------------------------
1. Download and extract the Shiphero module into the modules directory (usually "sites/all/modules").
2. Go to "Administer" -> "Modules" and enable the module.


After enabled the module should do the following steps
------------------------------------------------------
1. Step 1: 
    Go to following URL and Save the shiphero E-mail id , token  (in Admin loing )
    http://yousite.com/admin/commerce/shiphero
	
2. Step 2:
   To register url for update the order status. run the following url
   http://yousite.com/shipherregister      
